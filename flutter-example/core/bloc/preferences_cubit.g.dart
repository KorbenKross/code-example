// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preferences_cubit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PreferencesState _$_$_PreferencesStateFromJson(Map<String, dynamic> json) {
  return _$_PreferencesState(
    token: json['token'] as String,
    refreshToken: json['refreshToken'] as String,
  );
}

Map<String, dynamic> _$_$_PreferencesStateToJson(
        _$_PreferencesState instance) =>
    <String, dynamic>{
      'token': instance.token,
      'refreshToken': instance.refreshToken,
    };
