part of 'themes.dart';

abstract class _CColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color yellow = Color(0xFFF2AD1D);
  static const Color grey = Color(0xFF9C9EA5);
  static const Color greyDark = Color(0xFF404648);
  static const Color greyLight = Color(0xFF808189);
  static const Color appColor = Color(0xFF13253A);
  static const Color neutralColor = Color(0xFF2D6673);
  static const Color negativeColor = Color(0xFFB44C4C);
  static const Color positiveColor = Color(0xFF438524);
}