import 'package:flutter/foundation.dart';
import 'package:methronome_app/core/localization/locale_base.dart';

@immutable
class RuLocale implements BaseLocale {

  @override
  String get methronome => "MethroSound";
}