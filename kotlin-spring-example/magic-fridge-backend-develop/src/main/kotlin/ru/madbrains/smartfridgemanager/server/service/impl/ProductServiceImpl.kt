package ru.allerhand.smartfridgemanager.server.service.impl

import org.springframework.stereotype.Service
import ru.allerhand.smartfridgemanager.server.entity.ProductEntity
import ru.allerhand.smartfridgemanager.server.repository.ProductRepository
import ru.allerhand.smartfridgemanager.server.service.ProductService

@Service
class ProductServiceImpl(private val productRepository: ProductRepository) : ProductService {

    override fun getAll(): List<ProductEntity> = productRepository.findAll()

    override fun findByName(name: String) = productRepository.findByName(name)

    override fun findById(id: Int): ProductEntity? = productRepository.findById(id)
            .orElse(null)

    override fun delete(id: Int) = productRepository.deleteById(id)

    override fun save(productEntity: ProductEntity) {
        productRepository.save(productEntity)
    }
}