package ru.allerhand.smartfridgemanager.server.entity

enum class UserType(val type: String) {
    REGULAR("REGULAR"),
    PREMIUM("PREMIUM"),
    BLOCKED("BLOCKED"),
    UNCOMPLETED("UNCOMPLETED")
}