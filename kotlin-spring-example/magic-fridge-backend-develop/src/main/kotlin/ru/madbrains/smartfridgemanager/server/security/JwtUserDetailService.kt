package ru.allerhand.smartfridgemanager.server.security

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.allerhand.smartfridgemanager.server.service.UserService
import ru.allerhand.smartfridgemanager.server.utils.convertToJwt

@Service
class JwtUserDetailService(val userService: UserService) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val userEntity = userService.findByUsername(username)
                ?: throw UsernameNotFoundException("User not username $username not found!")
        return userEntity.convertToJwt()
    }
}