package ru.allerhand.smartfridgemanager.server.dto.request

data class UserRegisterRequestDto(val username: String, val password: String, val locale: String)