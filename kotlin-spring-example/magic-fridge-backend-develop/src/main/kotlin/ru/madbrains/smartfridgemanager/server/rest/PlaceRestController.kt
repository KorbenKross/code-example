package ru.allerhand.smartfridgemanager.server.rest

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.allerhand.smartfridgemanager.server.dto.response.PlaceLoadResponseDto
import ru.allerhand.smartfridgemanager.server.service.PlaceService


@RestController
@RequestMapping(value = ["/api/v1/place/"])
class PlaceRestController(val placeService: PlaceService) {

    /**
     * Загрузка стандартных мест хранения
     */
    @PostMapping("/load")
    fun load(): ResponseEntity<Any> {
        val places = placeService.getAll()
        return ResponseEntity.ok(PlaceLoadResponseDto(places))
    }

}