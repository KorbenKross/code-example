package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "product", schema = "public", catalog = "smartfridgemanagerdatabase")
open class ProductEntity {
    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "name", nullable = false)
    open var name: String? = null
    @Basic
    @Column(name = "image", nullable = true)
    open var image: ByteArray? = null
    @Basic
    @Column(name = "placeid", nullable = false, insertable = false, updatable = false)
    open var placeid: Int? = null
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "placeid", referencedColumnName = "id")
    open var refPlaceEntity: PlaceEntity? = null
    @OneToMany(mappedBy = "refProductEntity")
    open var refRecipeProductEntities: MutableList<RecipeProductEntity>? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "name = $name " +
                    "image = $image " +
                    "placeid = $placeid " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ProductEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (image != other.image) return false
        if (placeid != other.placeid) return false

        return true
    }

}

