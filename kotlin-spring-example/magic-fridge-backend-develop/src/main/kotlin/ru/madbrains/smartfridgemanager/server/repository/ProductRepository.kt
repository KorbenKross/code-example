package ru.allerhand.smartfridgemanager.server.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.allerhand.smartfridgemanager.server.entity.ProductEntity

@Repository
interface ProductRepository : JpaRepository<ProductEntity, Int> {

    fun findByName(name: String): ProductEntity?
}