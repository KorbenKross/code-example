package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.PersonEntity

interface PersonService {

    fun save(personEntity: PersonEntity): PersonEntity

    fun findByUserid(id: Int): PersonEntity?

    fun delete(id: Int)

    fun delete(personEntity: PersonEntity)
}