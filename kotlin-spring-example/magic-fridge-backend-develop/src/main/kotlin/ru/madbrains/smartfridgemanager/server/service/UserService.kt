package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.UserEntity

interface UserService {

    fun register(userEntity: UserEntity): UserEntity

    fun getAll(): List<UserEntity>

    fun findById(id: Int): UserEntity?

    fun findByUsername(username: String): UserEntity?

    fun delete(id: Int)

    fun save(userEntity: UserEntity)
}