package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "place", schema = "public", catalog = "smartfridgemanagerdatabase")
open class PlaceEntity {
    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "name", nullable = false)
    open var name: String? = null

    @OneToMany(mappedBy = "refPlaceEntity")
    open var refProductEntities: MutableList<ProductEntity>? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "name = $name " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as PlaceEntity

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

}

