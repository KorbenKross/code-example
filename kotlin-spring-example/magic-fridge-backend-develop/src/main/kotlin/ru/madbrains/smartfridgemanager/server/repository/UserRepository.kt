package ru.allerhand.smartfridgemanager.server.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.allerhand.smartfridgemanager.server.entity.UserEntity
import java.util.*

@Repository
interface UserRepository : JpaRepository<UserEntity, Int> {
    fun findByUsername(username: String): UserEntity?
}
