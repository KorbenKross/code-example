package ru.allerhand.smartfridgemanager.server.entity

import java.sql.Date
import javax.persistence.*

@Entity
@Table(name = "usr", schema = "public", catalog = "smartfridgemanagerdatabase")
open class UserEntity {
    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "username", nullable = false)
    open var username: String? = null
    @Basic
    @Column(name = "password", nullable = false)
    open var password: String? = null
    @Basic
    @Column(name = "typee", nullable = false)
    //REGULAR, PREMIUM OR BLOCKED (DELETED)
    open var type: String? = null
    @Basic
    @Column(name = "locale", nullable = false)
    open var locale: String? = null
    @Basic
    @Column(name = "lastLogIn", nullable = false)
    open var lastLogIn: Date? = null

//    @OneToMany(mappedBy = "refUserEntity")
//    open var refPersonEntities: MutableList<PersonEntity>? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "username = $username " +
                    "password = $password " +
                    "type = $type " +
                    "locale = $locale " +
                    "lastVisit = $lastLogIn " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as UserEntity

        if (id != other.id) return false
        if (username != other.username) return false
        if (password != other.password) return false
        if (type != other.type) return false
        if (locale != other.locale) return false
        if (lastLogIn != other.lastLogIn) return false

        return true
    }

}

