package ru.allerhand.smartfridgemanager.server.dto.response

import ru.allerhand.smartfridgemanager.server.entity.UserEntity
import java.sql.Date

data class PersonLoadResponseDto(val name: String,
                                 val birth: Date,
                                 val sex: String,
                                 val email: String,
                                 val user: UserEntity)