package ru.allerhand.smartfridgemanager.server.dto.request

data class UserLoginRequestDto(val username: String, val password: String)