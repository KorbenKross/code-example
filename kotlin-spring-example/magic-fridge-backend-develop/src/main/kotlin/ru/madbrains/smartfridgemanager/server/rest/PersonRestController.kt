package ru.allerhand.smartfridgemanager.server.rest

import org.springframework.http.ResponseEntity
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.allerhand.smartfridgemanager.server.dto.request.PersonLoadRequestDto
import ru.allerhand.smartfridgemanager.server.dto.request.PersonRegisterRequestDto
import ru.allerhand.smartfridgemanager.server.dto.request.RulesAcceptRequestDto
import ru.allerhand.smartfridgemanager.server.dto.response.PersonRegisterResponseDto
import ru.allerhand.smartfridgemanager.server.entity.PersonEntity
import ru.allerhand.smartfridgemanager.server.entity.UserType
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtTokenProvider
import ru.allerhand.smartfridgemanager.server.service.PersonService
import ru.allerhand.smartfridgemanager.server.service.UserService

@RestController
@RequestMapping(value = ["/api/v1/person/"])
class PersonRestController(val personService: PersonService,
                           val jwtTokenProvider: JwtTokenProvider,
                           val userService: UserService) {

    @PostMapping("/create")
    fun create(@RequestBody personRegisterRequestDto: PersonRegisterRequestDto): ResponseEntity<Any> {
        //TODO if token invalid or expired -> try/catch TokenException in filter
        val username = personRegisterRequestDto.userUsername
        val userEntity = userService.findByUsername(username)
        if (userEntity != null) {
            val oldPersonEntity = personService.findByUserid(userEntity.id!!)
            val personEntity = PersonEntity().apply {
                oldPersonEntity?.let { this.id = it.id }
                name = personRegisterRequestDto.name
                birth = personRegisterRequestDto.birth
                email = personRegisterRequestDto.email
                sex = personRegisterRequestDto.sex
                refUserEntity = userEntity
            }
            personService.save(personEntity)
            return ResponseEntity.ok(PersonRegisterResponseDto(
                    personEntity.name!!,
                    personEntity.birth!!,
                    personEntity.sex!!,
                    personEntity.email!!,
                    personEntity.refUserEntity!!))
        }
        throw UsernameNotFoundException("User with username $username not found!") //TODO to response
    }

    @PostMapping("/load")
    fun load(@RequestBody personLoadRequestDto: PersonLoadRequestDto): ResponseEntity<Any> {
        val user = userService.findByUsername(personLoadRequestDto.username)
        if (user != null) {
            personService.findByUserid(user.id!!)?.let {
                return ResponseEntity.ok(PersonRegisterResponseDto(
                        it.name!!, it.birth!!, it.sex!!, it.email!!, user))
            } ?: run { return ResponseEntity.status(403).body("Person not found") }
        }
        return ResponseEntity.status(403).body("User not found")
    }

    @PostMapping("/accept")
    fun acceptRules(@RequestBody rulesAcceptRequestDto: RulesAcceptRequestDto): ResponseEntity<Any> {
        val username = rulesAcceptRequestDto.username
        val userEntity = userService.findByUsername(username)
        userEntity?.let {
            it.type = UserType.REGULAR.type
            userService.save(it)
            val personEntity = personService.findByUserid(userEntity.id!!)
            //TODO RulesAcceptResponceDto
            personEntity?.let {
                return ResponseEntity.ok(personEntity)
            } ?: run {
                return ResponseEntity.status(404).body("Person not found")
            }
        }
        return ResponseEntity.status(404).body("User with username $username not found") //TODO Error
    }

//        val token = jwtTokenProvider.unwrap(bearerToken)
//        val username = jwtTokenProvider.getUsername(token.toString())
//        val userEntity =
//                if (userEntity != null) {
//                    personEntity.refUserEntity = userEntity
//                } else {
//                    throw UsernameNotFoundException("User with username $username not found")
//                }
}