package ru.allerhand.smartfridgemanager.server.dto.response

class ProductLoadRequestDto(val id: Int,
                            val name: String,
                            val image: ByteArray,
                            val placeId: Int)