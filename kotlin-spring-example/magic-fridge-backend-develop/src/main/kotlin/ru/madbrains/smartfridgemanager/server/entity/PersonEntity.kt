package ru.allerhand.smartfridgemanager.server.entity

import java.sql.Date
import javax.persistence.*

@Entity
@Table(name = "person", schema = "public", catalog = "smartfridgemanagerdatabase")
open class PersonEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "name", nullable = false)
    open var name: String? = null
    @Basic
    @Column(name = "birth", nullable = false)
    open var birth: Date? = null
    @Basic
    @Column(name = "sex", nullable = false)
    open var sex: String? = null
    @Basic
    @Column(name = "email", nullable = true)
    open var email: String? = null
    @Basic
    @Column(name = "ration", nullable = true)
    open var ration: String? = null
    @Basic
    @Column(name = "userid", nullable = false, insertable = false, updatable = false)
    open var userid: Int? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "id")
    open var refUserEntity: UserEntity? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "name = $name " +
                    "birth = $birth " +
                    "sex = $sex " +
                    "email = $email " +
                    "ration = $ration " +
                    "userid = $userid " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as PersonEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (birth != other.birth) return false
        if (sex != other.sex) return false
        if (email != other.email) return false
        if (ration != other.ration) return false
        if (userid != other.userid) return false

        return true
    }

}

