package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "recipe", schema = "public", catalog = "smartfridgemanagerdatabase")
open class RecipeEntity {
    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "name", nullable = false)
    open var name: String? = null
    @Basic
    @Column(name = "time", nullable = false)
    open var time: Int? = null
    @Basic
    @Column(name = "image", nullable = true)
    open var image: ByteArray? = null
    @Basic
    @Column(name = "mealplanid", nullable = false, insertable = false, updatable = false)
    open var mealplanid: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mealplanid", referencedColumnName = "id")
    open var refMealplanEntity: MealplanEntity? = null
    @OneToMany(mappedBy = "refRecipeEntity")
    open var refRecipeProductEntities: MutableList<RecipeProductEntity>? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "name = $name " +
                    "time = $time " +
                    "image = $image " +
                    "mealplanid = $mealplanid " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as RecipeEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (time != other.time) return false
        if (image != other.image) return false
        if (mealplanid != other.mealplanid) return false

        return true
    }

}

