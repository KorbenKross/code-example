package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "step", schema = "public", catalog = "smartfridgemanagerdatabase")
open class StepEntity {
    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    @Basic
    @Column(name = "title", nullable = false)
    open var title: String? = null
    @Basic
    @Column(name = "body", nullable = false)
    open var body: String? = null
    @Basic
    @Column(name = "image", nullable = true)
    open var image: ByteArray? = null
    @Basic
    @Column(name = "sequence_number", nullable = false)
    open var sequenceNumber: Int? = null
    @Basic
    @Column(name = "recipeid", nullable = false, insertable = false, updatable = false)
    open var recipeId: Int? = null
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipeid", referencedColumnName = "id")
    open var refRecipeEntity: RecipeEntity? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "title = $title " +
                    "body = $body " +
                    "image = $image " +
                    "sequenceNumber = $sequenceNumber " +
                    "recipeId = $recipeId " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as StepEntity

        if (id != other.id) return false
        if (title != other.title) return false
        if (body != other.body) return false
        if (recipeId != other.recipeId) return false

        return true
    }
}