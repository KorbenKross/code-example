package ru.allerhand.smartfridgemanager.server.service.impl

import org.springframework.stereotype.Service
import ru.allerhand.smartfridgemanager.server.entity.PersonEntity
import ru.allerhand.smartfridgemanager.server.repository.PersonRepository
import ru.allerhand.smartfridgemanager.server.service.PersonService

@Service
class PersonServiceImpl(val personRepository: PersonRepository) : PersonService {
    override fun save(personEntity: PersonEntity): PersonEntity {
        return personRepository.save(personEntity)
    }

    override fun findByUserid(id: Int): PersonEntity? {
        return personRepository.findByUserid(id)
    }

    override fun delete(id: Int) {
        return personRepository.deleteById(id)
    }

    override fun delete(personEntity: PersonEntity) {
        personRepository.delete(personEntity)
    }


}