package ru.allerhand.smartfridgemanager.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SmartFridgeServerApplication

fun main(args: Array<String>) {
	runApplication<SmartFridgeServerApplication>(*args)
}
