package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.StepEntity

interface StepService {

    fun getAll(): List<StepEntity>

    fun findById(id: Int): StepEntity?

    fun delete(id: Int)

    fun save(stepEntity: StepEntity)
}