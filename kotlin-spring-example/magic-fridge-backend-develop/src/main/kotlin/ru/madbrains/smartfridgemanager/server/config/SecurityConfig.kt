package ru.allerhand.smartfridgemanager.server.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtConfigurer
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtTokenProvider

@Configuration
class SecurityConfig(val jwtTokenProvider: JwtTokenProvider) : WebSecurityConfigurerAdapter() {

    companion object {
        const val LOGIN_ENDPOINT = "/api/v1/auth/login"
        const val REGISTER_ENDPOINT = "/api/v1/auth/register"
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()

    }

    override fun configure(http: HttpSecurity) {
        http.httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(REGISTER_ENDPOINT).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(JwtConfigurer(jwtTokenProvider))
    }

}