package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.PlaceEntity

interface PlaceService {

    fun getAll(): List<PlaceEntity>

    fun findByName(name: String): PlaceEntity?

    fun findById(id: Int): PlaceEntity?

    fun delete(id: Int)

    fun save(placeEntity: PlaceEntity)
}