package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "person_person", schema = "public", catalog = "smartfridgemanagerdatabase")
@IdClass(PersonPersonEntityPK::class)
open class PersonPersonEntity {
    @Id
    @Column(name = "personid", nullable = false)
    var personid: Int? = null
    @Id
    @Column(name = "personid2", nullable = false)
    var personid2: Int? = null


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "personid = $personid " +
                    "personid2 = $personid2 " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as PersonPersonEntity

        if (personid != other.personid) return false
        if (personid2 != other.personid2) return false

        return true
    }

}

class PersonPersonEntityPK : java.io.Serializable {
    @Id
    @Column(name = "personid", nullable = false)
    var personid: Int? = null
    @Id
    @Column(name = "personid2", nullable = false)
    var personid2: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as PersonPersonEntityPK

        if (personid != other.personid) return false
        if (personid2 != other.personid2) return false

        return true
    }

    override fun hashCode(): Int = 42
}
