package ru.allerhand.smartfridgemanager.server.dto.request

import com.fasterxml.jackson.annotation.JsonFormat
import java.sql.Date

data class PersonRegisterRequestDto(
        val name: String,
        @JsonFormat(pattern = "yyyy-MM-dd")
        val birth: Date,
        val sex: String,
        val email: String,
        val userUsername: String)