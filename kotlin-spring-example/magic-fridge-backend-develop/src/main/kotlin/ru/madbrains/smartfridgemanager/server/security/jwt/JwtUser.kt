package ru.allerhand.smartfridgemanager.server.security.jwt

import org.codehaus.jackson.annotate.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.sql.Date

class JwtUser(val id: Int,
              private val userUsername: String,
              private val userPassword: String,
              val userType: String,
              val userLocale: String,
              val userLastLogIn: Date
) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = mutableListOf(SimpleGrantedAuthority("USER"))

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = userUsername

    @JsonIgnore
    override fun isCredentialsNonExpired(): Boolean = true

    @JsonIgnore
    override fun getPassword(): String = userPassword

    @JsonIgnore
    override fun isAccountNonExpired(): Boolean = true

    @JsonIgnore
    override fun isAccountNonLocked(): Boolean = true
}