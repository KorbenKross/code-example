package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.ProductEntity

interface ProductService {

    fun getAll(): List<ProductEntity>

    fun findByName(name: String): ProductEntity?

    fun findById(id: Int): ProductEntity?

    fun delete(id: Int)

    fun save(productEntity: ProductEntity)


}