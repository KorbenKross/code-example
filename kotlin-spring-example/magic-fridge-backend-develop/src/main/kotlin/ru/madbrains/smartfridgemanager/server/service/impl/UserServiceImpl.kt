package ru.allerhand.smartfridgemanager.server.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import ru.allerhand.smartfridgemanager.server.entity.UserEntity
import ru.allerhand.smartfridgemanager.server.entity.UserType
import ru.allerhand.smartfridgemanager.server.repository.UserRepository
import ru.allerhand.smartfridgemanager.server.service.UserService
import java.sql.Date

@Service
class UserServiceImpl(val userRepository: UserRepository) : UserService {

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    override fun register(userEntity: UserEntity): UserEntity {
        userEntity.password = bCryptPasswordEncoder.encode(userEntity.password)
        userEntity.lastLogIn = Date(System.currentTimeMillis())
        userEntity.type = UserType.UNCOMPLETED.type
        return userRepository.save(userEntity)
    }

    override fun getAll(): List<UserEntity> = userRepository.findAll()

    override fun findById(id: Int): UserEntity? = userRepository.findById(id)
            .orElse(null)

    override fun findByUsername(username: String) = userRepository.findByUsername(username)

    override fun delete(id: Int) = userRepository.deleteById(id)

    override fun save(userEntity: UserEntity) {
        userRepository.save(userEntity)
    }
}