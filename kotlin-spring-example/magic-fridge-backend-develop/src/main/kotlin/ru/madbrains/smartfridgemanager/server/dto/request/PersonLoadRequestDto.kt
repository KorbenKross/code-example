package ru.allerhand.smartfridgemanager.server.dto.request

data class PersonLoadRequestDto(val username: String)