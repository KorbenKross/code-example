package ru.allerhand.smartfridgemanager.server.dto.request

data class RulesAcceptRequestDto(val username: String)