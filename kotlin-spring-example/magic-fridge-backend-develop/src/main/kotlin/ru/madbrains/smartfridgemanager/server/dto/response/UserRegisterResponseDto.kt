package ru.allerhand.smartfridgemanager.server.dto.response

import java.sql.Date

data class UserRegisterResponseDto(val id: Int,
                                   val username: String,
                                   val token: String,
                                   val type: String,
                                   val locale: String,
                                   val lastLogIn: Date)