package ru.allerhand.smartfridgemanager.server.service

import ru.allerhand.smartfridgemanager.server.entity.ProductEntity

interface RecipeService {
    fun getAll(): List<RecipeService>

    fun findByName(name: String): RecipeService?

    fun findById(id: Int): RecipeService?

    fun delete(id: Int)

    fun save(recipeEntity: RecipeService)
}