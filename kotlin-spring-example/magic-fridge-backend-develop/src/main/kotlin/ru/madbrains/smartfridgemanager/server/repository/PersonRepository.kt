package ru.allerhand.smartfridgemanager.server.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.allerhand.smartfridgemanager.server.entity.PersonEntity

@Repository
interface PersonRepository : JpaRepository<PersonEntity, Int> {

    fun findByUserid(id: Int): PersonEntity?

    override fun deleteById(id: Int)
}