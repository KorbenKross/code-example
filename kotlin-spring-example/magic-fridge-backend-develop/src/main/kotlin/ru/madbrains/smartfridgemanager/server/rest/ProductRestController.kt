package ru.allerhand.smartfridgemanager.server.rest

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.allerhand.smartfridgemanager.server.entity.ProductEntity
import ru.allerhand.smartfridgemanager.server.service.ProductService

@RestController
@RequestMapping(value = ["/api/v1/product/"])
class ProductRestController(val productService: ProductService) {

    /**
     * Загрузка стандартных продуктов
     */
    @PostMapping("/load")
    fun load(): ResponseEntity<Any> {
        var products = ArrayList<ProductEntity>()
        products.add(ProductEntity().apply {
            id = 1
            name = "Яйца"
            image = ByteArray(10)
            placeid = 1
        })
        products.add(ProductEntity().apply {
            id = 2
            name = "Молоко"
            image = ByteArray(10)
            placeid = 1
        })
        products.add(ProductEntity().apply {
            id = 3
            name = "Курица"
            image = ByteArray(10)
            placeid = 2
        })
        return ResponseEntity.ok(products) //TODO change to response
//        throw UsernameNotFoundException("User with username $username not found!") //TODO to response
    }
}