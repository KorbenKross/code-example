package ru.allerhand.smartfridgemanager.server.rest

import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.allerhand.smartfridgemanager.server.dto.request.UserLoginRequestDto
import ru.allerhand.smartfridgemanager.server.dto.request.UserRegisterRequestDto
import ru.allerhand.smartfridgemanager.server.dto.response.UserLoginResponseDto
import ru.allerhand.smartfridgemanager.server.dto.response.UserRegisterResponseDto
import ru.allerhand.smartfridgemanager.server.entity.UserEntity
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtAuthenticationException
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtTokenProvider
import ru.allerhand.smartfridgemanager.server.service.UserService

@RestController
@RequestMapping(value = ["/api/v1/auth/"])
class AuthenticationRestController(
        val authenticationManager: AuthenticationManager,
        val jwtTokenProvider: JwtTokenProvider,
        val userService: UserService) {


    @PostMapping("/login")
    fun login(@RequestBody userLoginRequestDto: UserLoginRequestDto): ResponseEntity<Any> {
        try {
            val username = userLoginRequestDto.username
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username,
                    userLoginRequestDto.password))
            val userEntity = userService.findByUsername(username)
                    ?: throw UsernameNotFoundException("User with username $username not found")
            val token = jwtTokenProvider.createToken(username)

            val response = UserLoginResponseDto(
                    userEntity.id!!,
                    userEntity.username!!,
                    token,
                    userEntity.type!!,
                    userEntity.locale!!,
                    userEntity.lastLogIn!!)
            //TODO change lastLoginTime
//            val response = HashMap<Any, Any>()
//            response["username"] = username
//            response["token"] = token
            return ResponseEntity.ok(response)

        } catch (e: JwtAuthenticationException) {
            throw BadCredentialsException("Invalid username or password")
        }
    }

    @PostMapping("/register")
    fun register(@RequestBody userRegisterRequestDto: UserRegisterRequestDto): ResponseEntity<Any> {
        val username = userRegisterRequestDto.username
        val userEntity = userService.findByUsername(username)
        if (userEntity == null) {
            val userEntity = UserEntity().apply {
                this.username = userRegisterRequestDto.username
                this.password = userRegisterRequestDto.password
                this.locale = userRegisterRequestDto.locale
            }
            //TODO Validate username and location and make some Exceptions
            val registeredUser = userService.register(userEntity)
            val token = jwtTokenProvider.createToken(registeredUser.username!!)
            val userRegisterResponseDto = UserRegisterResponseDto(
                    userEntity.id!!,
                    registeredUser.username!!,
                    token,
                    registeredUser.type!!,
                    registeredUser.locale!!,
                    registeredUser.lastLogIn!!
            )
            return ResponseEntity.ok(userRegisterResponseDto)
        } else {
            throw UsernameNotFoundException("User with username $username not found")
        }
    }
}