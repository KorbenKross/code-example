package ru.allerhand.smartfridgemanager.server.service.impl

import org.springframework.stereotype.Service
import ru.allerhand.smartfridgemanager.server.entity.PlaceEntity
import ru.allerhand.smartfridgemanager.server.repository.PlaceRepository
import ru.allerhand.smartfridgemanager.server.service.PlaceService
@Service
class PlaceServiceImpl(private val placeRepository: PlaceRepository) : PlaceService {

    override fun getAll(): List<PlaceEntity> = placeRepository.findAll()

    override fun findByName(name: String) = placeRepository.findByName(name)

    override fun findById(id: Int): PlaceEntity? = placeRepository.findById(id)
            .orElse(null)

    override fun delete(id: Int) = placeRepository.deleteById(id)

    override fun save(placeEntity: PlaceEntity) {
        placeRepository.save(placeEntity)
    }
}