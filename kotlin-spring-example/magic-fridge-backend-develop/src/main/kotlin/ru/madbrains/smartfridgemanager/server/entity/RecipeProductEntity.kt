package ru.allerhand.smartfridgemanager.server.entity

import javax.persistence.*

@Entity
@Table(name = "recipe_product", schema = "public", catalog = "smartfridgemanagerdatabase")
@IdClass(RecipeProductEntityPK::class)
open class RecipeProductEntity {
    @Id
    @Column(name = "recipeid", nullable = false, insertable = false, updatable = false)
    var recipeid: Int? = null
    @Id
    @Column(name = "productid", nullable = false, insertable = false, updatable = false)
    var productid: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipeid", referencedColumnName = "id")
    open var refRecipeEntity: RecipeEntity? = null
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productid", referencedColumnName = "id")
    open var refProductEntity: ProductEntity? = null

    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "recipeid = $recipeid " +
                    "productid = $productid " +
                    ")"

    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as RecipeProductEntity

        if (recipeid != other.recipeid) return false
        if (productid != other.productid) return false

        return true
    }

}

class RecipeProductEntityPK : java.io.Serializable {
    @Id
    @Column(name = "recipeid", nullable = false, insertable = false, updatable = false)
    var recipeid: Int? = null
    @Id
    @Column(name = "productid", nullable = false, insertable = false, updatable = false)
    var productid: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as RecipeProductEntityPK

        if (recipeid != other.recipeid) return false
        if (productid != other.productid) return false

        return true
    }

    override fun hashCode(): Int = 42

}
