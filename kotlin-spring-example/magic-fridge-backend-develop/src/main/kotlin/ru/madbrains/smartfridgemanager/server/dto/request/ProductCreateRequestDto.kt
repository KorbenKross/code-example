package ru.allerhand.smartfridgemanager.server.dto.request

data class ProductCreateRequestDto(val id: Int,
                                   val name: String,
                                   val amount: Int,
                                   val amountMeasure: String,
                                   val cost: Int,
                                   val costMeasure: String,
                                   val description: String,
                                   val barcode: String,
                                   val image: ByteArray,
                                   val placeId: Int) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProductCreateRequestDto

        if (id != other.id) return false

        return true
    }

    override fun hashCode() = id
}