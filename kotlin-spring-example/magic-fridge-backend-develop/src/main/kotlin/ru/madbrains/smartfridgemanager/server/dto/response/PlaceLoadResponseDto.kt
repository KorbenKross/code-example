package ru.allerhand.smartfridgemanager.server.dto.response

import ru.allerhand.smartfridgemanager.server.entity.PlaceEntity

data class PlaceLoadResponseDto(val places: List<PlaceEntity>)