package ru.allerhand.smartfridgemanager.server.utils

import ru.allerhand.smartfridgemanager.server.entity.UserEntity
import ru.allerhand.smartfridgemanager.server.security.jwt.JwtUser

fun UserEntity.convertToJwt(): JwtUser {
    return JwtUser(
            this.id!!.toInt(),
            this.username!!,
            this.password!!,
            this.type!!,
            this.locale!!,
            this.lastLogIn!!)
}