package sandlabs.app.com.mokat_test.mvp.basket;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import ru.evotor.devices.commons.DeviceServiceConnector;
import ru.evotor.devices.commons.exception.DeviceServiceException;
import ru.evotor.devices.commons.exception.ServiceNotConnectedException;
import ru.evotor.devices.commons.printer.PrinterDocument;
import ru.evotor.devices.commons.printer.printable.PrintableText;
import ru.evotor.devices.commons.services.IPrinterServiceWrapper;
import ru.allerhand.app.com.mokat_test.R;
import ru.allerhand.app.com.mokat_test.adapters.BasketAdapter;
import ru.allerhand.app.com.mokat_test.api.model.requests.RentSessionRequestModelApi;
import ru.allerhand.app.com.mokat_test.api.model.response.CustomerResponseModelApi;
import ru.allerhand.app.com.mokat_test.base.BaseFragment;
import ru.allerhand.app.com.mokat_test.db.models.Basket;
import ru.allerhand.app.com.mokat_test.model.PledgeEnum;
import ru.allerhand.app.com.mokat_test.model.product.ExtraKind;
import ru.allerhand.app.com.mokat_test.model.product.Kind;
import ru.allerhand.app.com.mokat_test.mvp.category.CategoryFragment_;
import ru.allerhand.app.com.mokat_test.mvp.customer.CustomersForSellFragment;
import ru.allerhand.app.com.mokat_test.mvp.customer.CustomersForSellFragment_;
import ru.allerhand.app.com.mokat_test.utils.DateUtils;
import ru.allerhand.app.com.mokat_test.utils.ReceiptUtils;

import static ru.evotor.devices.commons.Constants.DEFAULT_DEVICE_INDEX;
import static ru.evotor.devices.commons.DeviceServiceConnector.getPrinterService;
import static sandlabs.app.com.mokat_test.utils.StaticConstants.BASKET_LIMIT;

@EFragment(R.layout.fragment_basket)
public class BasketFragment
        extends BaseFragment
        implements BasketContract.BasketView,
        BasketAdapter.ItemClickListener,
        BasketAdapter.CancelItemClickListener,
        CustomersForSellFragment.OnCustomerForSellSelectedListener,
        AdapterView.OnItemSelectedListener {

    @Inject
    BasketContract.BasketPresenter presenter;

    private final String ZERO_RESULT = "0";
    private BasketAdapter productBasketAdapter;
    private List<Basket> productBasketList;
    private CustomerResponseModelApi customerResponseModelApi;
    private StringBuilder fullCustomerName;
    private String pledgeType;
    private String pledgeComment;
    private List<String> pledgeList;

    @ViewById(R.id.products_cart_recycler_view)
    RecyclerView recyclerViewProductCart;

    @ViewById(R.id.sum_result_from_basket_text_view)
    TextView sumResultBasketTextView;

    @ViewById(R.id.customer_full_name_text)
    EditText customerFullNameTextView;

    @ViewById(R.id.add_product_for_basket_button)
    Button addProductForBasketButton;

    @ViewById(R.id.basket_rent_session_pledge)
    Spinner basketRentSessionPledgeSpinner;

    @Click(R.id.clean_basket_customer_full_name)
    void cleanBasketCustomerFullName() {
        customerFullNameTextView.setText("");
        this.customerResponseModelApi = null;
    }

    @Click(R.id.add_product_for_basket_button)
    void addProductForBasketOnClick() {
        if (productBasketAdapter.getItemCount() < BASKET_LIMIT) {
            CategoryFragment_ productFragment_ = new CategoryFragment_();
            FragmentTransaction transaction1 = Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction1.replace(R.id.input_container, productFragment_)
                    .addToBackStack(null)
                    .commit();
        } else {
            this.showToast(R.string.max_limit_basket_products);
        }
    }

    @Click(R.id.customer_full_name_text)
    void openCustomersForSellFragmentOnClick() {
        CustomersForSellFragment_ customersFragment = new CustomersForSellFragment_();
        customersFragment.setOnCustomerForSellSelectedListener(this);
        FragmentTransaction transaction =
                Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.input_container, customersFragment)
                .addToBackStack(null)
                .commit();
    }

    @Click(R.id.open_rent_session_button)
    void openRentSessionOnClick() {
        if (fullCustomerName != null) {
            if (pledgeComment != null && pledgeType != null
                    && !pledgeComment.equals("") && !pledgeType.equals("")) {
                if (productBasketAdapter.getItemCount() == 0) {
                    showToast(R.string.basket_emty_alert);
                    return;
                }
                presenter.openRentSessionList(
                        productBasketAdapter.getBasketProductList(),
                        Double.parseDouble(sumResultBasketTextView.getText().toString()),
                        fullCustomerName.toString(),
                        pledgeType
                );
            } else {
                this.showToast(R.string.fill_pledge_type);
            }
        } else {
            customerFullNameTextView.setError(getString(R.string.warning_custome_empty_data));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeviceServiceConnector.startInitConnections(getContext());
    }

    @AfterViews
    void init() {
        initiateSpinner();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerViewProductCart.setLayoutManager(layoutManager);
        productBasketList = new ArrayList<>();
        productBasketAdapter = new BasketAdapter(getContext(), productBasketList);
        productBasketAdapter.setClickListener(this);
        productBasketAdapter.setCancelItemClickListener(this);
        recyclerViewProductCart.setAdapter(productBasketAdapter);

        presenter.loadChoosenProductsFormDb();
        loadToolbar();
    }

    private void initiateSpinner() {
        pledgeList = new ArrayList<>();
        for (PledgeEnum value : PledgeEnum.values()) {
            pledgeList.add(value.getPledge());
        }
        basketRentSessionPledgeSpinner.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(
                Objects.requireNonNull(getContext()),
                android.R.layout.simple_spinner_item,
                pledgeList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(Color.GRAY);
                } else {
                    textView.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        basketRentSessionPledgeSpinner.setAdapter(arrayAdapter);
        if (!presenter.getPledgeTypeWithComment().equals("")) {
            for (String pledge : pledgeList) {
                if (pledge.equals(presenter.getPledgeType())) {
                    basketRentSessionPledgeSpinner.setSelection(pledgeList.indexOf(pledge));
                    pledgeType = presenter.getPledgeTypeWithComment();
                    pledgeComment = presenter.getPledgeComment();
                    break;
                }
            }
        }
    }

    @Override
    public void setProductsFromCart(List<Basket> productsFromCart) {
        this.productBasketList = productsFromCart;
        productBasketAdapter.setItems(productBasketList);
        recyclerViewProductCart.setAdapter(productBasketAdapter);
        calculateTotal();
    }

    // TODO: 30.10.2019 Скопировать на главную активность для отображения счетчика корзины сразу после запуска
    private void loadToolbar() {
        Toolbar toolbar =
                (Toolbar) Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        if (toolbar != null) {
            TextView basketProductsCounter =
                    ((TextView) getActivity().findViewById(R.id.add_product_for_basket_counter));
            int basketProductsCount = presenter.getBasketProductsCount();
            if (basketProductsCount != 0) {
                basketProductsCounter.setVisibility(View.VISIBLE);
                basketProductsCounter.setText(String.valueOf(presenter.getBasketProductsCount()));
            } else {
                basketProductsCounter.setText(String.valueOf(0));
                basketProductsCounter.setVisibility(View.GONE);
            }
        }
    }

    private void calculateTotal() {
        if (productBasketAdapter.getItemCount() > 0) {
            sumResultBasketTextView.setText(productBasketAdapter.getBasketItemsSumResult());
        } else {
            sumResultBasketTextView.setText(ZERO_RESULT);
        }
    }

    /*
       ------------------
       Прокат велосипедов
         "Прокат Мокат"
          Сессия №1234
       Продавец: Иван П.П
       Покупатель: Ирина Л.Л
       -------------------
      1. Велосипед Скоростной
      (Инв. №1234) - 150 руб/ч
      2. Велосипед Городской
      (Инв. №5678) - 110 руб/ч
       -------------------
       Залог: ПР.ми
       Дата начала проката
         01/10/2019 10:10
            Штрих-код

      |||||||||||||||||||||
    */

    @Override
    public void printPreReceipt(List<RentSessionRequestModelApi> rentSessionResponseModelApiList) {
        // TODO: 06.10.2019 После добавления штриха вынести в презентер
        new Thread(() -> {
            IPrinterServiceWrapper printerService = null;
            try {
                printerService = getPrinterService();
            } catch (ServiceNotConnectedException e) {
                e.printStackTrace();
            }

            int allowableSymbols = 0;
            try {
                if (printerService != null) {
                    allowableSymbols = printerService
                            .getAllowableSymbolsLineLength(DEFAULT_DEVICE_INDEX);
                }
            } catch (DeviceServiceException e) {
                e.printStackTrace();
            }

            StringBuilder stringBuilderPreReceipt = new StringBuilder();
            stringBuilderPreReceipt.append(ReceiptUtils.getReceiptLine(allowableSymbols));
            stringBuilderPreReceipt.append(ReceiptUtils.getCenteredLine(allowableSymbols, "Прокат велосипедов"));
            stringBuilderPreReceipt.append(ReceiptUtils.getCenteredLine(allowableSymbols, "\"Прокат\""));
//            stringBuilderPreReceipt.append(presenter.getCenteredLine(allowableSymbols,"Сессия #1234")); // TODO: 03.10.2019 ID сессии и штрих код на чеке
            stringBuilderPreReceipt.append(
                    "Продавец: " + rentSessionResponseModelApiList.get(0).getSellerUsername() + "\n");
            stringBuilderPreReceipt.append(
                    "Покупатель: " + fullCustomerName + "\n");
            stringBuilderPreReceipt.append(ReceiptUtils.getReceiptLine(allowableSymbols));
            for (int i = 0; i < productBasketList.size(); i++) {
                Basket basket = productBasketList.get(i);
                stringBuilderPreReceipt.append(i + ". " + Kind.valueOf(basket.kind).getKind() +
                        " - " + ExtraKind.valueOf(basket.extraKind).getName() + " - "
                        + "Инв.№" + basket.inventNumber + " (" + basket.cost + "руб/ч)" + "\n");
            }
            stringBuilderPreReceipt.append(ReceiptUtils.getReceiptLine(allowableSymbols));
            stringBuilderPreReceipt.append("Залог: " + presenter.getPledgeType() + "/" +
                    presenter.getPledgeComment() + "\n");
            stringBuilderPreReceipt.append(ReceiptUtils.getCenteredLine(allowableSymbols, "Дата начала проката"));
            stringBuilderPreReceipt.append(ReceiptUtils.getCenteredLine(allowableSymbols, DateUtils.getDate()));
            stringBuilderPreReceipt.append(ReceiptUtils.getReceiptLine(allowableSymbols));
            try {
                getPrinterService().printDocument(
                        DEFAULT_DEVICE_INDEX,
                        new PrinterDocument(
                                new PrintableText(stringBuilderPreReceipt.toString())));
            } catch (DeviceServiceException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public void cleanBasket() {
        loadToolbar();
        productBasketAdapter.clearBasket();
        presenter.setFullCustomerName("");
        presenter.setPledgeTypeWithComment("");
    }

    @Override
    public void onItemClick(View view, int position) {
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCancelItemClick(int productBasketId, int position) {
        presenter.deleteProductBasketByIdFromDb(productBasketId);
        sumResultBasketTextView.setText((int) productBasketAdapter.getRecalculateTotal(position) + "");
        loadToolbar();
    }

    @Override
    public void onCustomerForSellSelected(CustomerResponseModelApi customerResponseModelApi) {
        this.customerResponseModelApi = customerResponseModelApi;
        fullCustomerName = new StringBuilder();
        fullCustomerName
                .append(customerResponseModelApi.getFirstName())
                .append(" ")
                .append(customerResponseModelApi.getLastName())
                .append(" ")
                .append(customerResponseModelApi.getPatronymic());
        presenter.setFullCustomerName(fullCustomerName.toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fullCustomerName != null
                && !fullCustomerName.toString().equals("")
                && !presenter.getFullCustomerName().equals("")) {
            fullCustomerName = new StringBuilder(presenter.getFullCustomerName());
            customerFullNameTextView.setText(fullCustomerName.toString());
            customerFullNameTextView.setError(null);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            if (pledgeList != null) {
                if (!pledgeList.get(position).equals(presenter.getPledgeType())) {
                    String[] spinnerItems = new String[pledgeList.size()];
                    pledgeList.toArray(spinnerItems);
                    pledgeType = spinnerItems[position];
                    pledgeComment = "";
                    DialogSetupPledgeFragment dialogSetupPledgeFragment_ = new DialogSetupPledgeFragment_();
                    dialogSetupPledgeFragment_
                            .setOnDialogListener(new DialogSetupPledgeFragment.OnDialogListener() {
                                @Override
                                public void onFinishDialog(String comment) {
                                    pledgeComment = comment;
                                    pledgeType += "~" + comment;
                                    presenter.setPledgeTypeWithComment(pledgeType);
                                    dialogSetupPledgeFragment_.dismiss();
                                }

                                @Override
                                public void onCloseDialog() {
                                    dialogSetupPledgeFragment_.dismiss();
                                    if (pledgeComment == null || pledgeComment.equals("")) {
                                        presenter.setPledgeTypeWithComment("");
                                        initiateSpinner();
                                    }
                                }
                            });
                    dialogSetupPledgeFragment_.show(Objects.requireNonNull(getActivity())
                            .getSupportFragmentManager(), this.getClass().getName());
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        System.out.println();
    }
}
