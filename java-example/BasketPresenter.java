package sandlabs.app.com.mokat_test.mvp.basket;

import android.text.format.DateFormat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import ru.allerhand.app.com.mokat_test.R;
import ru.allerhand.app.com.mokat_test.api.model.requests.RentSessionRequestModelApi;
import ru.allerhand.app.com.mokat_test.base.BasePresenter;
import ru.allerhand.app.com.mokat_test.db.models.Basket;
import ru.allerhand.app.com.mokat_test.model.rent.RentSessionRequestModel;
import ru.allerhand.app.com.mokat_test.repository.DatabaseRepository;
import ru.allerhand.app.com.mokat_test.repository.NetworkableRepository;
import ru.allerhand.app.com.mokat_test.repository.PreferenceRepository;

public class BasketPresenter
        extends BasePresenter
        implements BasketContract.BasketPresenter {

    NetworkableRepository networkableRepository;
    BasketContract.BasketView basketView;
    PreferenceRepository preferenceRepository;
    DatabaseRepository databaseRepository;

    private CompositeDisposable disposable = new CompositeDisposable();
    private PublishSubject<RentSessionRequestModel> openRentSessionListSubject = PublishSubject.create();
    private List<RentSessionRequestModelApi> rentSessionRequestModelApiList;
    private String fullCustomerName;
    private String lastCustomerName;

    public BasketPresenter(
            NetworkableRepository networkableRepository,
            BasketContract.BasketView basketView,
            PreferenceRepository preferenceRepository,
            DatabaseRepository databaseRepository
    ) {
        this.networkableRepository = networkableRepository;
        this.basketView = basketView;
        this.preferenceRepository = preferenceRepository;
        this.databaseRepository = databaseRepository;

        disposable.add(openRentSessionListSubject
                        .doOnNext(unused ->
                                this.basketView.showProgressDialog(R.string.authorization_in_progress))
                        .flatMap(rentSessionRequestModel -> {
                            rentSessionRequestModelApiList =
                                    rentSessionRequestModel.getRentSessionResponseModelList();
                            return this.networkableRepository
                                    .requestAddRentSessionList(
                                            rentSessionRequestModel.getRentSessionResponseModelList(),
                                            rentSessionRequestModel.getCustomerLastName(),
                                            preferenceRepository.getCurrentUsername(),
                                            rentSessionRequestModel.getTotalCost(),
                                            rentSessionRequestModel.getPledgeType()
                                    )
                                    .subscribeOn(Schedulers.io());
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(responseList -> {
                            this.basketView.showToast(R.string.success_product_to_rent);
                            this.basketView.hideProgressDialog();
                            cleanBasket();
                        }, ex -> {
                            this.basketView.hideProgressDialog();
                            Log.e(BasketPresenter.class.getName(), ex.getMessage());
                            disposable.clear();
                        })
        );
    }

    private void cleanBasket() {
        databaseRepository.deleteAllProductFromCart();
        this.basketView.cleanBasket();
    }

    @Override
    public void onDestroy() {
        disposable.clear();
    }

    @Override
    public void loadChoosenProductsFormDb() {
        basketView.setProductsFromCart(databaseRepository.getProductsFromCart());
    }

    @Override
    public void openRentSessionList(
            List<Basket> basketProductList,
            Double totalCost,
            String fullCustomerName,
            String pledgeType
    ) {
        this.fullCustomerName = fullCustomerName;
        String currentDateSessionOpen = String.valueOf(
                DateFormat.format(
                        "yyyy-MM-dd'T'HH:mm:ss",
                        Calendar.getInstance().getTime())
        );
        List<RentSessionRequestModelApi> rentSessionRequestModelApiList = new ArrayList<>();
        for (Basket basket : basketProductList) {
            rentSessionRequestModelApiList.add(
                    new RentSessionRequestModelApi(
                            currentDateSessionOpen,
                            "",
                            true,
                            basket.cost != null ? basket.cost : 0.0,
                            0.0,
                            parseCustomerLastName(fullCustomerName),
                            preferenceRepository.getCurrentUsername(),
                            basket.inventNumber
                    )
            );
        }
        basketView.printPreReceipt(rentSessionRequestModelApiList);
        RentSessionRequestModel rentSessionRequestModel = new RentSessionRequestModel(
                rentSessionRequestModelApiList,
                lastCustomerName,
                totalCost,
                pledgeType
        );
        openRentSessionListSubject.onNext(rentSessionRequestModel);
    }

    @Override
    public void deleteProductBasketByIdFromDb(int productBasketId) {
        databaseRepository.deleteProductFromCartById(productBasketId);
    }

    @Override
    public int getBasketProductsCount() {
        return databaseRepository.getProductsFromCart().size();
    }

    @Override
    public void setPledgeTypeWithComment(String pledgeTypeWithComment) {
        preferenceRepository.setPledgeTypeWithComment(pledgeTypeWithComment);
    }

    @Override
    public String getPledgeType() {
        return preferenceRepository.getPledgeTypeWithComment().split("~")[0];
    }

    @Override
    public String getPledgeComment() {
        return preferenceRepository.getPledgeTypeWithComment().split("~")[1];
    }

    @Override
    public String getPledgeTypeWithComment() {
        return preferenceRepository.getPledgeTypeWithComment();
    }

    @Override
    public void setFullCustomerName(String customerFullname) {
        preferenceRepository.setCustomerFullname(customerFullname);
    }

    @Override
    public String getFullCustomerName() {
        return preferenceRepository.getCustomerFullname();
    }

    private String parseCustomerLastName(String fullCustomerName) {
        String[] fullNameArray = fullCustomerName.split(" ");
        lastCustomerName = fullNameArray[1];
        return fullNameArray[1];
    }
}
