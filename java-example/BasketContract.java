package sandlabs.app.com.mokat_test.mvp.basket;

import java.util.List;

import ru.allerhand.app.com.mokat_test.api.model.requests.RentSessionRequestModelApi;
import ru.allerhand.app.com.mokat_test.base.BaseContract;
import ru.allerhand.app.com.mokat_test.db.models.Basket;

public interface BasketContract {

    interface BasketView extends BaseContract.BaseView {

        void setProductsFromCart(List<Basket> productsFromCart);

        void printPreReceipt(List<RentSessionRequestModelApi> rentSessionRequestModelApiList);

        void cleanBasket();
    }

    interface BasketPresenter extends BaseContract.BasePresenter {

        void loadChoosenProductsFormDb();

        void openRentSessionList(
                List<Basket> basketProductList,
                Double totalCost,
                String fullCustomerName,
                String pledgeType
        );

        void deleteProductBasketByIdFromDb(int productBasketId);

        int getBasketProductsCount();

        void setPledgeTypeWithComment(String pledgeTypeWithComment);

        String getPledgeType();

        String getPledgeComment();

        String getPledgeTypeWithComment();

        void setFullCustomerName(String customerFullname);

        String getFullCustomerName();
    }

    interface DialogSetupPledgeView extends BaseContract.BaseView {

    }

    interface DialogSetupPledgePresenter extends BaseContract.BasePresenter {
    }
}
